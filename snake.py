import curses
import time
import random

def redraw():
    """
    Перерисовка игрового пространства для каждого хода
    """
    win.erase()     # Очистка рабочего окна
    draw_food()     # Прорисовка еды
    draw_snake()    # Прорисовка змейки
    draw_menu()     # Прорисовка меню
    win.refresh()   # Обновление окна


def draw_menu():
    """
    Прорисовка игрового меню
    """
    win.addstr(0, 0, "Счет: " + str(len(snake) - 2) + "\t\tНажмите 'q' для выхода", curses.color_pair(0))


def draw_snake():
    """
    Прорисовка змейки
    """
    try:
        n = 0
        for pos in snake:  # Змейка как массив(лист) [x, y], поэтому мы меняем их местами ниже
            if n == 0:
                win.addstr(pos[1], pos[0], "@", curses.color_pair(0))  # Рисуем голову
            else:
                win.addstr(pos[1], pos[0], "#", curses.color_pair(0))  # Рисуем части тела
            n += 1
    # Обработка ошибки прорисовки
    except Exception as drawing_error:
        print(drawing_error, str(cols), str(rows))


def draw_food():
    """
    Прорисовка еды на игровом поле
    """
    for pos in food:
        win.addstr(pos[1], pos[0], "+", curses.color_pair(0))


def drop_food():
    """
    Спавним еду на игровом поле
    """
    x = random.randint(1, cols - 2)
    y = random.randint(1, rows - 2)
    for pos in snake:  # Не размещать еду на змейке
        if pos == [x, y]:
            drop_food()  # вызываем еще раз
    # добавляем еду в массив
    food.append([x, y])


def move_snake():
    """
    Игровая логика движения змейки
    """
    global snake             # Змейка(Список)
    global grow_snake        # Рост змейки
    global cols, rows        # Счетчики

    head = snake[0]          # Обозначаем голову змейки
    if not grow_snake:       # Убираем хвост, если еда не была съедена за этот ход
        snake.pop()
    else:                    # Если еда была съедена за этот ход, не убирать последний объект листа
        grow_snake = False   # Восстанавливаем состояние по-умолчанию для роста змейки
    # движение вверх
    if direction == DIR_UP:  # Просчет позиции головы
        head = [head[0], head[1] - 1]  # Меняем x и y местами в прорисовке змейки draw_snake()
        if head[1] == 0:
            head[1] = rows - 2  # Змейка проходит через стены
    # движение вниз
    elif direction == DIR_DOWN:
        head = [head[0], head[1] + 1]
        if head[1] == rows - 1:
            head[1] = 1
    # движение влево
    elif direction == DIR_LEFT:
        head = [head[0] - 1, head[1]]
        if head[0] == 0:
            head[0] = cols - 2
    # движение вправо
    elif direction == DIR_RIGHT:
        head = [head[0] + 1, head[1]]
        if head[0] == cols - 1:
            head[0] = 1

    snake.insert(0, head)  # Добавляем новую голову


def is_food_collision():
    """
    Проверка коллизии с едой
    :return: bool
    """
    for pos in food:
        if pos == snake[0]:
            food.remove(pos)
            return True
    return False


def game_over():
    """
    Игровая логика окончания игры.
    """
    global is_game_over
    is_game_over = True
    win.erase()
    win.addstr(10, 20, "Игра окончена. Ваш счет " + str(len(snake)) + ". Нажмите 'q' для выхода.", curses.color_pair(0))


def is_suicide():
    """
    Проверка коллизии с самим собой.
    :return: bool
    """
    for i in range(1, len(snake)):
        if snake[i] == snake[0]:
            return True
    return False


def end_game():
    """
    Окончание игровой сессии, завершение работы с терминалом
    """
    curses.nocbreak()
    win.keypad(False)
    curses.echo()
    curses.endwin()


# Начало инициализации
DIR_UP = 0  # Значения/ключи для указания направления змейки
DIR_RIGHT = 1
DIR_DOWN = 2
DIR_LEFT = 3

is_game_over = False  # семафор окончания игры
grow_snake = False  # семафор определения роста змейки

snake = [[10, 5], [9, 5]]  # размер и расположение змейки
direction = DIR_RIGHT  # Начальный вектор направления змейки
food = []  # список еды
win = curses.initscr()  # инициализаця консольного игрового окружения используя модуль curses
curses.start_color()  # подключение цветов
win.keypad(True)  # подключение кнопок ввода(вверх, вниз, вправо, влево)
win.nodelay(True)  # отключение задержки обработки ввода
curses.curs_set(0)  # спрятать курсор
curses.cbreak()  # Считывание ввод мгновенно
curses.noecho()  # Не выводить информацию о нажатиях
rows, cols = win.getmaxyx()  # Получение информации о размере консольного окна
# Конец инициализации


if __name__ == '__main__':
    # спавним еду
    drop_food()
    # перерисовка окружения
    redraw()

    # запуск игрового цикла
    while True:
        # пока игра не закончилась
        if is_game_over is False:
            redraw()
        key = win.getch()  # Получение значения при нажатии
        time.sleep(0.1)  # Скорость игры

        if key != -1:  # обработка нажатых клавиш
            # поворот навер
            if key == curses.KEY_UP:
                if direction != DIR_DOWN:  # Змейка не может подниматься вверх, если она пошла вниз
                    direction = DIR_UP
            # поворот направо
            elif key == curses.KEY_RIGHT:
                if direction != DIR_LEFT:
                    direction = DIR_RIGHT
            # поворот вниз
            elif key == curses.KEY_DOWN:
                if direction != DIR_UP:
                    direction = DIR_DOWN
            # поворот налево
            elif key == curses.KEY_LEFT:
                if direction != DIR_RIGHT:
                    direction = DIR_LEFT
            elif chr(key) == "q":
                break

        # если не конец игры, то продолжаем игру
        if is_game_over is False:
            move_snake()

        # если змейка умерла
        if is_suicide():
            game_over()

        # если встретились с едой
        if is_food_collision():
            drop_food()
            grow_snake = True

    # заканчиваем игру
    end_game()
